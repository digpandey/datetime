import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatetmComponent } from './datetm.component';

describe('DatetmComponent', () => {
  let component: DatetmComponent;
  let fixture: ComponentFixture<DatetmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatetmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatetmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
